![propolis](https://ptpimg.me/31x482.png)

# Propolis

# Installation
1. Navigate to the [repository homepage](https://gitlab.com/passelecasque/propolis) on Gitlab.
2. In the sidebar on the left, click [CI/CD](https://gitlab.com/passelecasque/propolis/-/pipelines).
3. Underneath [CI/CD](https://gitlab.com/passelecasque/propolis/-/pipelines), click [Pipelines](https://gitlab.com/passelecasque/propolis/-/pipelines).
4. In the list of pipelines, click on the three dots to the right of the top-most pipeline.
4. Download the artifacts by clicking the button underneath **Download artifacts**
5. Extract the contents of the archive into a new folder.
6. (Optional, but recommended) Add the folder to your PATH environment variable.
7. If you are on Windows, run the executable along with the path to your album with `.\propolis_windows.exe <path to album>`
7. If you are on MacOS, run the executable along with the path to your album with `./propolis_darwin <path to album>`
7. If you are on Linux, run the executable along with the path to your album with `./propolis <path to album>`

### Notes:
- Don't include the angle brackets when specifying the path to your album.
- Don't include a trailing slash or backslash when specifying the path to your album.
- If the path to your album includes spaces, surround the path with double quotes.

# Compiling
1. Install `go`, `sox`, `flac`, and `git` with a package manager (`scoop` or `choco` on Windows, `brew` on MacOS, or `apt` or `dnf` on Linux) and put them in PATH env var
2. `git clone https://gitlab.com/passelecasque/propolis`
3. `cd propolis/cmd/propolis`

**If you're using Windows Powershell:**

4. `$env:GOOS='windows'`
5. `$env:GOARCH='amd64'`
6. `go build -ldflags "-X main.Version=0.5.2" -o ../../propolis_windows.exe`
7. `cd ../..`
8. There should be a `propolis_windows.exe` file in the current directory. Add it to your PATH env var

**If you're using Linux:**

4. `go build -trimpath -ldflags "-X main.Version=0.5.2" -o ../../propolis`
5. `cd ../..`
6. There should be a `propolis` file in the current directory. Add it to your PATH env var

**If you're using MacOS:**

4. `GOOS=darwin GOARCH=amd64 go build -ldflags "-X main.Version=0.5.2" -o ../../propolis_darwin`
5. `cd ../..`
6. There should be a `propolis_darwin` file in the current directory. Add it to your PATH env var

